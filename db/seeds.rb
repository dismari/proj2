# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


u = User.create(id:1)

r1 = u.recipes.create(name: 'Test1')
r2 = u.recipes.create(name: 'Test2')
r3 = u.recipes.create(name: 'Test3')
r4 = u.recipes.create(name: 'Test4')

r1.ingredients.create(name: 'Beef')
r1.ingredients.create(name: 'Brocoli')
r1.ingredients.create(name: 'Rice')
r1.ingredients.create(name: 'Soy Sauce')
r1.ingredients.create(name: 'Carrots')


r2.ingredients.create(name: 'Chicken')
r2.ingredients.create(name: 'Mushrooms')
r2.ingredients.create(name: 'Green Beans')
r2.ingredients.create(name: 'Rice')


r3.ingredients.create(name: 'Pork')
r3.ingredients.create(name: 'Beans')
r3.ingredients.create(name: 'Potatoes')
r3.ingredients.create(name: 'Apple Sauce')


r4.ingredients.create(name: 'Banannas')
r4.ingredients.create(name: 'Pecans')
r4.ingredients.create(name: 'Rum')
r4.ingredients.create(name: 'Brown Sugar')
r4.ingredients.create(name: 'Butter')
r4.ingredients.create(name: 'Ice Cream')
