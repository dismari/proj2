class RecipesController < ApplicationController
  before_action :set_recipe, only: [:show, :edit, :update, :destroy]

  def index
    @recipes = User.find(1).recipes
  end

def create
  @user = User.find(1)
  flash[:notice] = "Recipe has been created!"
  redirect_to user_path(@user)
end

def show
end

def new
  @user = User.find(1)
  @recipe = @user.recipes.create
end

def edit
  @user = User.find(1)
end

def update
  @user = User.find(1)
  if @recipe.update(recipe_params)
    redirect_to user_recipe_path(@user, @recipe )
  end
end

def destroy
  @user = User.find(params[:user_id])
  @recipe = @user.recipes.destroy
  redirect_to user_path(@user)
end


private
  def set_recipe
    @recipe = Recipe.find(params[:id])
  end
 

  def recipe_params
    params.require(:recipe).permit(:name, :ingredients_attributes => [:id, :name, :_destroy])
  end

end

