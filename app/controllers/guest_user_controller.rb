class GuestUserController < ApplicationController
  before_action :set_guest_user, only:[:show]

  def show
    recipes = @Guest_user.recipes
    @random_recipe = recipes[rand(recipes.length)]
  end


  private
    def set_guest_user
      @Guest_user = User.find(1)
    end
  

end
