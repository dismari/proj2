json.array!(@recipes) do |recipe|
  json.extract! recipe, :id, :name, :img_path
  json.url recipe_url(recipe, format: :json)
end
