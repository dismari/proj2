json.array!(@ingredients) do |ingredient|
  json.extract! ingredient, :id, :name, :img_path
  json.url ingredient_url(ingredient, format: :json)
end
