class Recipe < ActiveRecord::Base

  has_many :ingredients
  belongs_to :user
  accepts_nested_attributes_for :ingredients, allow_destroy: true

end
